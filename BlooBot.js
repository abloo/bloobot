const Discord = require("discord.js");
const client = new Discord.Client();
const config = require("./config.json");
const fs = require("fs");

// fs.readdir("./events/", (err, files) => {
//   if (err) return console.error(err);
//   files.forEach(file => {
//     let eventFunction = require(`./events/${file}`);
//     let eventName = file.split(".")[0];
//     // super-secret recipe to call events with all their proper arguments *after* the `client` var.
//     client.on(eventName, (...args) => eventFunction.run(client, ...args));
//   });
// });


client.on("error", (e) => console.error(e));
client.on("warn", (e) => console.warn(e));
client.on("debug", (e) => console.info(e));
client.login(config.token);


client.on("ready", () => {
  console.log("I am ready!");
  // Set the client user's status
  client.user.setActivity('Bloo', { type: 'LISTENING' });
});


//command handler
client.on("message", (message) => {
  if (message.author.bot) return;
  if(message.content.indexOf(config.prefix) !== 0) return;
  if(!message.channel.isPrivate){
    //owner commands
    if (message.author.id == config.ownerID){
      const args = message.content.slice(config.prefix.length).trim().split(/ +/g);
      const command = args.shift().toLowerCase();

      try {
        let commandFile = require(`./commands/global/${command}.js`);
        commandFile.run(client, message, args);
      } catch(err){
        try {
          let commandFile = require(`./commands/owner/${command}.js`);
          commandFile.run(client, message, args);
        } catch(err){
          console.log(err);
        }
      }
    }
    //non owner
    else {
      const args = message.content.slice(config.prefix.length).trim().split(/ +/g);
      const command = args.shift().toLowerCase();

      try {
        let commandFile = require(`./commands/global/${command}.js`);
        commandFile.run(client, message, args);
      } catch (err) {
        console.error(err);
      }
    }
  }
});

//reply array
client.on("message", (message) => {
  let msg = message.content.toLowerCase();
  if (message.author.bot) return;
  var reply = JSON.parse(fs.readFileSync('reply.json'));
  if(reply[message.guild.id][msg]){
    message.channel.send(reply[message.guild.id][msg]);
  }
});

client.on("message", (message) => {
  if (message.author.bot) return;
  if(!message.mentions.members.first()) return;
  if(message.mentions.members.first().id==client.user.id){
    const embed = new Discord.MessageEmbed()
      .setTitle("bloos personal shitposting bot.")
      .setColor("#FFB6C1")
      .setAuthor('bloobot', client.user.avatarURL(), 'https://gitlab.com/bloouwu/bloobot')
      .setDescription(`My prefix is: \`${config.prefix}\`\n
                      Type ~help for a command list`)
      .setFooter('Made by bloo#0860');
    message.channel.send({embed});
  }
});
