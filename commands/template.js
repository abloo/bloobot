const Discord = require("discord.js");
const config = require("../../config.json");

exports.run = (client, message, args) => {
  if (args.length == 0){
    const embed = new Discord.MessageEmbed()
      .setTitle("COMMANDNAME")
      .setColor("#COLOUR")
      .setDescription("COMMANDDESCRIPTION\n**Parameters:**")
      .addField("PARAMETER1",
        "PARAMETER DESCRIPTION", true)
      .addField("PARAMETER2",
        "PARAMETER DESCRIPTION", true);
    message.channel.send({embed});
  }
};
