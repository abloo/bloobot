const Discord = require("discord.js");
const fs = require('fs');

exports.run = (client, message, args) => {
  var commandlist = "\n";
  var fileNames = fs.readdirSync("./commands/global");
  const embed = new Discord.MessageEmbed()
    .setTitle("**Command List**")
    .setColor("#00B2EE");

    for (var i = 0; i < fileNames.length; i++) {
      if (fileNames[i] != "help.js"){
        commandlist +=(fileNames[i].charAt(0).toUpperCase() + fileNames[i].slice(1, -3) + "\n");
      }
    }
  embed.setDescription(commandlist);
  message.channel.send({embed});
};