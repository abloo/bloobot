const Discord = require("discord.js");
const fs = require("fs");
const config = require("../../config.json");
var reply = JSON.parse(fs.readFileSync('./reply.json'));  //parse the JSON

exports.run = (client, message, args) => {
  if (args.length == 0){
    const embed = new Discord.MessageEmbed()
      .setTitle("alias")
      .setColor("#A0F684")
      .setDescription("Create a reply to a message for me to say\n\n**Options:**")
      .addFields(
        { name: '\u200B', value:'create', inline: true},
        { name: '\u200B', value:'remove', inline: true},
        { name: '\u200B', value:'list', inline: true}
      );
    message.channel.send({embed});
  }
  if (args.length == 1){
    if(args[0] == "list"){
      let aliaslist = "";
      for(var i in reply[message.guild.id]){
        if((aliaslist.length + i.length + reply[message.guild.id][i].length + 3) < 2000 ){
          aliaslist+=(i+" : "+reply[message.guild.id][i]+"\n");
        }
        else{
          const listembed = new Discord.MessageEmbed()
            .setTitle("Alias List:")
            .setColor("#A0F684")
            .setDescription(aliaslist);
          message.channel.send(listembed);
          aliaslist = "";
          aliaslist+=(i+" : "+reply[message.guild.id][i]+"\n");
        }
      }
      const listembed = new Discord.MessageEmbed()
        .setTitle("Alias List:")
        .setColor("#A0F684")
        .setDescription(aliaslist);
      message.channel.send(listembed);
    } 
    else{
      const embed = new Discord.MessageEmbed()
        .setTitle("alias")
        .setColor("#A0F684")
        .setDescription("**Options:**\n*(must be seperated by `;`)*")
        .addField("<message>;", "The message to reply to", true)
        .addField(";<reply>", "The reply message", true);
      message.channel.send({embed});
    }
  }
  else if(args[0] == "create" || args[0] == "add"){
    var createargs = message.content.slice(8+args[0].length).split(";");
    if(createargs[1].charAt(0)==config.prefix){
      message.channel.send("fuck you");
    }
    else if((createargs[0].trim().length + createargs[1].trim().length) > 1997){
      message.channel.send("fuck you, too long");
    }
    else {
      if(!reply[message.guild.id]){
        reply[message.guild.id] = "";
      }
      reply[message.guild.id][createargs[0].trim()] = createargs[1].trim();

      fs.writeFileSync('reply.json', JSON.stringify(reply));
      message.channel.send("Alias succesfully created!");
    }
  }
  else if(args[0] == "remove" || args[0] == "del" || args[0] == "delete"){
    if(!reply[message.guild.id][args.slice(1).join(" ")]){
      message.channel.send("This alias does not exist!");
    }
    else{
    delete reply[message.guild.id][args.slice(1).join(" ")];
      // Push it onto the end of the existing comments
      fs.writeFileSync('reply.json', JSON.stringify(reply));
      message.channel.send("Alias succesfully removed!");
    }
  }
};
